#
# (c) Copyright 2017 Olliver Schinagl <o.schinagl@ultimaker.com>
#
# SPDX-License-Identifier:	AGPL-3.0+
#
# GPIO's are connected as follows:
#
# D-Sub 25	GPIO		Netname		Comment
# 01				5V0		Not connected, used as source
# 02				3V3		Used as power sink, always on
# 03		GPIO3_27	ATM_SPI_nRST	Atmel SPI/ICSP
# 04		GPIO2_23	ATM_SPI_CLK	Atmel SPI/ICSP
# 05		GPIO2_25	ATM_SPI_MISO	Atmel SPI/ICSP
# 06				GND		Ground
# 07				spare		Not connected, spare pin
# 08		GPIO1_01	BUZ_IN_SND	Speaker output
# 09		GPIO1_04	ROT_OUT_SE1	Rotary encoder
# 10		GPIO3_21	D&L_I2C_SCL	Display and light I2C bus
# 11		GPIO3_28	D&L_I2C_SDA	Display and light I2C bus
# 12		GPIO4_12	NFC_I2C_SCL	NFC I2C bus
# 13		GPIO4_13	NFC_I2C_SDA	NFC I2C bus
# 14				GND		Ground
# 15				spare		Not connected, spare pin
# 16		GPIO3_25	ATM_SPI_SS	Atmel SPI/ICSP
# 17		GPIO2_24	ATM_SPI_MOSI	Atmel SPI/ICSP
# 18		GPIO1_07	ATM_SER_RX	Atmel serial communication
# 19		GPIO1_08	ATM_SER_TX	Atmel serial communication
# 20		GPIO1_25	DIS_IN_RST	Display reset
# 21		GPIO3_31	ROT_OUT_SEC	Rotary encoder
# 22		GPIO7_13	ROT_OUT_SE2	Rotary encoder
# 23				GND		Ground
# 24				spare		Not connected, spare pin
# 25				GND		Ground

# Setup
setenv db25_pins "GPIO3_27 GPIO2_23 GPIO2_25 GPIO1_01 GPIO1_04 GPIO3_21 GPIO3_28 GPIO4_12 GPIO4_13 GPIO3_25 GPIO2_24 GPIO1_07 GPIO1_08 GPIO1_25 GPIO3_31 GPIO7_13"

# Setup gpio mux
mw.l 0x20e00f8 0x5 # GPIO2_23
mw.l 0x20e00fc 0x5 # GPIO2_24
mw.l 0x20e0100 0x5 # GPIO2_25
mw.l 0x20e00a4 0x5 # GPIO3_21
mw.l 0x20e00c4 0x5 # GPIO3_28
mw.l 0x20e0210 0x5 # GPIO4_12
mw.l 0x20e0214 0x5 # GPIO4_13

setenv clear_all 'for pin in ${db25_pins}; do gpio clear ${pin}; done'
setenv set_all 'for pin in ${db25_pins}; do gpio set ${pin}; done'

setenv flash_all 'run clear_all; sleep 1; run set_all; sleep 1'
setenv flash_one 'run clear_all; sleep 1; for pin in ${db25_pins}; do gpio set ${pin}; sleep 1; gpio clear ${pin}; done'
setenv set_stair 'run clear_all; sleep 1; for pin in ${db25_pins}; do gpio set ${pin}; sleep 1; done'
setenv clear_stair 'run set_all; for pin in ${db25_pins}; do gpio clear ${pin}; sleep 1; done'

setenv test_all 'while true; do run flash_all; run flash_all; run flash_one; run clear_stair; run set_stair; done'

run test_all
