setenv mmcdev 2
setenv bm mmc ${mmcdev}
setenv lr 0x10800000
setenv fdt_high 0xffffffff
setenv fr 0x15000000
setenv fdt imx6qp-ultimaker-um_next.dtb
setenv zi zImage-um
setenv bootm_low 0x15000000
load ${bm} ${lr} ${zi}
load ${bm} ${fr} ${fdt}

setenv bootargs 'console=ttymxc3,115200 root=/dev/mmcblk0p2 rootfstype=f2fs rw rootwait earlyprintk'
bootz ${lr} - ${fr}
