#
# (c) Copyright 2017 Olliver Schinagl <o.schinagl@ultimaker.com>
#
# SPDX-License-Identifier:	AGPL-3.0+
#
# GPIO's are connected as follows:
#
# D-Sub 25	GPIO		Netname		Comment
# 01				5V0		Not connected, used as source
# 02				3V3		Used as power sink, always on
# 03		gpio5_15	ATM_SPI_nRST	Atmel SPI/ICSP
# 04		gpio	ATM_SPI_CLK	Atmel SPI/ICSP
# 05		gpio	ATM_SPI_MISO	Atmel SPI/ICSP
# 06		    	GND		Ground
# 07		    	spare		Not connected, spare pin
# 08		gpio	BUZ_IN_SND	Speaker output
# 09		gpio	ROT_OUT_SE1	Rotary encoder
# 10		gpio	D&L_I2C_SCL	Display and light I2C bus
# 11		gpio	D&L_I2C_SDA	Display and light I2C bus
# 12		gpio	NFC_I2C_SCL	NFC I2C bus
# 13		gpio	NFC_I2C_SDA	NFC I2C bus
# 14		    	GND		Ground
# 15		    	spare		Not connected, spare pin
# 16		gpio	ATM_SPI_SS	Atmel SPI/ICSP
# 17		gpio	ATM_SPI_MOSI	Atmel SPI/ICSP
# 18		gpio	ATM_SER_RX	Atmel serial communication
# 19		gpio	ATM_SER_TX	Atmel serial communication
# 20		gpio	DIS_IN_RST	Display reset
# 21		gpio	ROT_OUT_SEC	Rotary encoder
# 22		gpio	ROT_OUT_SE2	Rotary encoder
# 23				GND		Ground
# 24				spare		Not connected, spare pin
# 25				GND		Ground

# Setup
setenv db25_pins ""

# Setup gpio mux

setenv clear_all 'for pin in ${db25_pins}; do gpio clear ${pin}; done'
setenv set_all 'for pin in ${db25_pins}; do gpio set ${pin}; done'

setenv flash_all 'run clear_all; sleep 1; run set_all; sleep 1'
setenv flash_one 'run clear_all; sleep 1; for pin in ${db25_pins}; do gpio set ${pin}; sleep 1; gpio clear ${pin}; done'
setenv set_stair 'run clear_all; sleep 1; for pin in ${db25_pins}; do gpio set ${pin}; sleep 1; done'
setenv clear_stair 'run set_all; for pin in ${db25_pins}; do gpio clear ${pin}; sleep 1; done'

setenv test_all 'while true; do run flash_all; run flash_all; run flash_one; run clear_stair; run set_stair; done'

run test_all
