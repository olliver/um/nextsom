# setenv fdt_board "eval-v3"
setenv fdt_board "imx7d-ultimaker-um_next"

# setenv fdt_file '${soc}-colibri-${fdt_board}.dtb'
setenv fdt_file '${fdt_board}.dtb'

setenv bootcmd 'run sdboot; setenv fdtfile ${fdt_file} && run distro_bootcmd;'

setenv nfsboot 'run setup; setenv bootargs ${defargs} ${nfsargs} ${setupargs} ${vidargs}; echo Booting from NFS...;dhcp ${kernel_addr_r} && tftp ${fdt_addr_r} ${fdt_file} && run fdt_fixup && bootz ${kernel_addr_r} - ${fdt_addr_r}'

setenv sdboot 'run setup; setenv bootargs ${defargs} ${sdargs} ${setupargs} ${vidargs}; echo Booting from MMC/SD card...; run m4boot && load mmc 0:1 ${kernel_addr_r} ${kernel_file} && load mmc 0:1 ${fdt_addr_r} ${fdt_file} && run fdt_fixup && bootz ${kernel_addr_r} - ${fdt_addr_r}'

saveenv
