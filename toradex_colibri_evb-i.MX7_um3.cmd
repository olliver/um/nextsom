#
# (c) Copyright 2017 Olliver Schinagl <o.schinagl@ultimaker.com>
#
# SPDX-License-Identifier:	AGPL-3.0+
#
# GPIO's are connected as follows:
#
# D-Sub 25	GPIO		Netname		Comment
# 01				5V0		Not connected, used as source
# 02				3V3		Used as power sink, always on
# 03		GPIO2_10	ATM_SPI_nRST	Atmel SPI/ICSP
# 04		GPIO4_10	ATM_SPI_CLK	Atmel SPI/ICSP
# 05		GPIO4_08	ATM_SPI_MISO	Atmel SPI/ICSP
# 06		    	GND		Ground
# 07		    	spare		Not connected, spare pin
# 08		GPIO1_08	BUZ_IN_SND	Speaker output
# 09		GPIO2_28	ROT_OUT_SE1	Rotary encoder
# 10		GPIO4_02	D&L_I2C_SCL	Display and light I2C bus
# 11		GPIO4_03	D&L_I2C_SDA	Display and light I2C bus
# 12		GPIO4_12	NFC_I2C_SCL	NFC I2C bus
# 13		GPIO4_13	NFC_I2C_SDA	NFC I2C bus
# 14		    	GND		Ground
# 15		    	spare		Not connected, spare pin
# 16		GPIO4_11	ATM_SPI_SS	Atmel SPI/ICSP
# 17		GPIO4_09	ATM_SPI_MOSI	Atmel SPI/ICSP
# 18		GPIO4_04	ATM_SER_RX	Atmel serial communication
# 19		GPIO4_05	ATM_SER_TX	Atmel serial communication
# 20		GPIO2_22	DIS_IN_RST	Display reset
# 21		GPIO2_15	ROT_OUT_SEC	Rotary encoder
# 22		GPIO2_26	ROT_OUT_SE2	Rotary encoder
# 23				GND		Ground
# 24				spare		Not connected, spare pin
# 25				GND		Ground

# Setup
setenv db25_pins "GPIO2_10 GPIO4_10 GPIO4_08 GPIO1_08 GPIO2_28 GPIO4_02 GPIO4_03 GPIO4_12 GPIO4_13 GPIO4_11 GPIO4_09 GPIO4_04 GPIO4_05 GPIO2_22 GPIO2_15 GPIO2_26"

setenv clear_all 'for pin in ${db25_pins}; do gpio clear ${pin}; done'
setenv set_all 'for pin in ${db25_pins}; do gpio set ${pin}; done'

setenv flash_all 'run clear_all; sleep 1; run set_all; sleep 1'
setenv flash_one 'run clear_all; sleep 1; for pin in ${db25_pins}; do gpio set ${pin}; sleep 1; gpio clear ${pin}; done'
setenv set_stair 'run clear_all; sleep 1; for pin in ${db25_pins}; do gpio set ${pin}; sleep 1; done'
setenv clear_stair 'run set_all; for pin in ${db25_pins}; do gpio clear ${pin}; sleep 1; done'

setenv test_all 'while true; do run flash_all; run flash_all; run flash_one; run clear_stair; run set_stair; done'

run test_all
